﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePosition : MonoBehaviour 
{

	Vector3 initialPosition;
	Vector3 newPosition;

	public static bool changePosition;

	void Start()
	{
		initialPosition = this.gameObject.transform.position;
	}

	public void OnMouseDown()
	{
		if (DragObject.popUpBool) 
		{
			print ("change position");
			this.gameObject.transform.position = new Vector3 (-5f, 0, -2f);
			changePosition = true;
			FindObjectOfType<PlayerPrefsAnalysis>().SetObject (transform.name, true);
		}
	}
}
