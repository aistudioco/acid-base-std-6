﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour 
{
	public GameObject activeObj, activeObj2, disableObj;
	Vector3 obje1Pos, obje2Pos;

	void Start()
	{
		obje1Pos = activeObj.transform.position;
		obje2Pos = activeObj2.transform.position;
	}

	public void OnMouseDown()
	{
		activeObj.transform.position = obje1Pos;
		activeObj2.transform.position = obje2Pos;

		activeObj.SetActive (true);
		activeObj2.SetActive (true);
		disableObj.SetActive (false);

		FindObjectOfType<General> ().OnObjectDrag (this.gameObject.name);

		print ("Active : " + activeObj.name);
		print ("Deactive : " + disableObj.name);
	}
}
