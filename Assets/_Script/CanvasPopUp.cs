﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasPopUp : MonoBehaviour 
{
	public GameObject canvasPopup1;
	public GameObject canvasPopup2;

	public void CanvasPopUpDisplay()
	{
		print ("Canvas PopUp1");

		if (ChangePosition.changePosition && DragObject.popUpBool) 
		{
			print ("Canvas PopUp");
			canvasPopup1.SetActive (true);
		}
	}

	public void OnCanvas1ButtonClick()
	{
		canvasPopup1.SetActive (false);
		canvasPopup2.SetActive (true);
	}

	public void OnCanvas2ButtonClick()
	{
		canvasPopup2.SetActive (false);
	}
}
