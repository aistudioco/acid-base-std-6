﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragObject : MonoBehaviour 
{
	Vector3 dist;
	float posX;
	float posY;
	Vector3 originalPosition;

	public static bool acidicObject = false, baseObject = false, nutralObject = false, redLitmusPaper = false, blueLitmusPaper = false;

	public bool acidObj, baseObj, nutralObj;
	public static bool popUpBool = false;

	public bool trigger;

	string solutionName;
	public GameObject prefab;
	public GameObject bowl_Parent;

	int i = 1;

	void Start()
	{
		originalPosition = transform.position;
	}

	void OnMouseDown () 
	{
		dist = Camera.main.WorldToScreenPoint (transform.position);
		posX = Input.mousePosition.x - dist.x;
		posY = Input.mousePosition.y - dist.y;
	}

	void OnMouseDrag () 
	{
		Vector3 curPos = new Vector3 (Input.mousePosition.x - posX, Input.mousePosition.y - posY, dist.z);
		Vector3 worldPos = Camera.main.ScreenToWorldPoint (curPos);
		transform.position = worldPos;
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag ("Bowl_Parent")) 
		{
			print ("Mouse up");
		}

		// Acidic Object

		if (col.gameObject.CompareTag ("Lemon") || col.gameObject.CompareTag ("Tomato")) 
		{
			if (col.gameObject.CompareTag ("Lemon")) 
			{
				transform.name = "Lemon-Bowl";
			}
			if (col.gameObject.CompareTag ("Tomato")) 
			{
				transform.name = "Tomato-Bowl";
			}
			acidicObject = true;
			baseObject = false;
			nutralObject = false;

			solutionName = col.tag + " water";
			print (solutionName);

			col.gameObject.SetActive (false);

			FindObjectOfType<General> ().ObjectWater ();
			FindObjectOfType<General> ().changeCanvasName (solutionName);
		}

		if (col.gameObject.CompareTag ("Butter Milk") || col.gameObject.CompareTag ("Curd") || col.gameObject.CompareTag ("Tamarind") || col.gameObject.CompareTag("Citrate")) 
		{
			if (col.gameObject.CompareTag ("Butter Milk")) 
			{
				transform.name = "Butter Milk-Bowl";
			}
			if (col.gameObject.CompareTag ("Curd")) 
			{
				transform.name = "Curd-Bowl";
			}
			if (col.gameObject.CompareTag ("Tamarind")) 
			{
				transform.name = "Tamarind-Bowl";
			}
			if (col.gameObject.CompareTag ("Citrate")) 
			{
				transform.name = "Citrate-Bowl";
			}

			acidicObject = true;
			baseObject = false;
			nutralObject = false;

			solutionName = col.tag + " water";
			print (solutionName);

			col.transform.GetChild (0).gameObject.SetActive(false);

			FindObjectOfType<General> ().ObjectWater ();
			FindObjectOfType<General> ().changeCanvasName (solutionName);
		}

		// Basic Object
			
		if (col.gameObject.CompareTag ("Edible Soda") || col.gameObject.CompareTag ("Lime") || col.gameObject.CompareTag ("Washing Soda")) 
		{
			if (col.gameObject.CompareTag ("Edible Soda")) 
			{
				transform.name = "Edible Soda-Bowl";
			}
			if (col.gameObject.CompareTag ("Lime")) 
			{
				transform.name = "Lime-Bowl";
			}
			if (col.gameObject.CompareTag ("Washing Soda")) 
			{
				transform.name = "Washing Soda-Bowl";
			}

			acidicObject = false;
			baseObject = true;
			nutralObject = false;

			solutionName = col.tag + " water";
			print (solutionName);

			col.transform.GetChild (0).gameObject.SetActive(false);

			FindObjectOfType<General> ().ObjectWater ();
			FindObjectOfType<General> ().changeCanvasName (solutionName);
		}

		if (col.gameObject.CompareTag ("Soap")) 
		{
			if (col.gameObject.CompareTag ("Soap")) 
			{
				transform.name = "Soap-Bowl";
			}

			acidicObject = false;
			baseObject = true;
			nutralObject = false;

			solutionName = col.tag + " water";
			print (solutionName);
			col.gameObject.SetActive (false);

			FindObjectOfType<General> ().ObjectWater ();
			FindObjectOfType<General> ().changeCanvasName (solutionName);
		}

		// Nutral Object

		if (col.gameObject.CompareTag ("Sugar") || col.gameObject.CompareTag ("Salt")) 
		{
			if (col.gameObject.CompareTag ("Sugar")) 
			{
				transform.name = "Sugar-Bowl";
			}
			if (col.gameObject.CompareTag ("Salt")) 
			{
				transform.name = "Salt-Bowl";
			}

			acidicObject = false;
			baseObject = false;
			nutralObject = true;

			solutionName = col.gameObject.name + " water";
			print (solutionName);

			col.transform.GetChild (0).gameObject.SetActive(false);

			FindObjectOfType<General> ().ObjectWater ();
			FindObjectOfType<General> ().changeCanvasName (solutionName);
		}

		// Litmus Paper

		if (col.gameObject.CompareTag ("Red Litmus Paper")) 
		{
			General.i++;
			redLitmusPaper = true;
			FindObjectOfType<General> ().CheckEffect ();
		}

		if (col.gameObject.CompareTag ("Blue Litmus Paper")) 
		{
			General.i++;
			blueLitmusPaper = true;
			FindObjectOfType<General> ().CheckEffect ();
		}

		// Instantiate Bowl

		if (col.gameObject.CompareTag ("Empty_Bowl")) 
		{
			GameObject emptyBowl;
			print (i);
			col.name = transform.name;
			if (acidicObject) 
			{
				col.GetComponent<DragObject>().acidObj = true;
				col.gameObject.transform.tag = "Bowl_Parent";
			}

			if (baseObject) 
			{
				col.GetComponent<DragObject>().baseObj = true;
				col.gameObject.transform.tag = "Bowl_Parent";
			}

			if (nutralObject) 
			{
				col.GetComponent<DragObject> ().nutralObj = true;
				col.gameObject.transform.tag = "Bowl_Parent";
			}
				
			col.transform.GetChild (0).gameObject.SetActive(true);
			col.transform.GetChild (2).gameObject.SetActive(true);


			print ("Water Move");

			if (i < 4 && col.GetComponent<DragObject> ().trigger != true ) {
				emptyBowl = Instantiate (prefab, col.gameObject.transform.position + new Vector3 (0, 0, -4f), Quaternion.identity);
				i++;
			}

			else if (i == 4 && col.GetComponent<DragObject> ().trigger != true ) {
				emptyBowl = Instantiate (prefab, new Vector3 (8.5f, 0, 5f), Quaternion.identity);
				i++;
			}

			else if (i > 4 && i < 8 && col.GetComponent<DragObject> ().trigger != true ) {
				emptyBowl = Instantiate (prefab, col.gameObject.transform.position + new Vector3 (0, 0, -4f), Quaternion.identity);
				i++;
			}

			else if (i == 8 && col.GetComponent<DragObject> ().trigger != true ) {
				emptyBowl = Instantiate (prefab, new Vector3 (12f, 0, 5f), Quaternion.identity);
				i++;
			}

			else if (i > 8 && i < 12 && col.GetComponent<DragObject> ().trigger != true ) {
				emptyBowl = Instantiate (prefab, col.gameObject.transform.position + new Vector3 (0, 0, -4f), Quaternion.identity);
				i++;
			}

			col.transform.GetChild (2).GetChild (1).GetComponent<Text> ().text = solutionName;

			col.GetComponent<DragObject> ().trigger = true;
			FindObjectOfType<General> ().ResetCanvasName ();
		}
	}

	void OnMouseUp()
	{
		if (!popUpBool) 
		{
			transform.position = originalPosition;
		}

		if (i == 12) 
		{
			bowl_Parent.SetActive (false);
			popUpBool = true;
		}
	}
}