﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsAnalysis : MonoBehaviour 
{

	public static bool isLemonAnalysis, isSugarAnalysis, isLimeAnalysis, isEdibleSodaAnalysis, isButterMilkAnalysis, isTomatoAnalysis, isSoapAnalysis, 
						isSaltAnalysis, isWashingSodaAnalysis, isCurdAnalysis, isTamarindAnalysis, isCitrateAnalysis;

	static string activeObject = "";

	bool activeFromBowl = false;

	public void SetObject(string bowlName, bool isBowl = true) 
	{
		if (isBowl) 
		{
			activeFromBowl = true;
			var objName = bowlName.Split('-');
			SetObject (objName [0].Trim ());
		}
	}

	public void SetObject(string objName)
	{
		ResetObject ();
		switch (objName) 
		{

		case "Lemon": 
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnMenuButtonClick (objName);
			}
			isLemonAnalysis = true;
			activeObject = objName;
			Debug.Log (objName + " set as active.");
			break;
		
		case "Sugar": 
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isSugarAnalysis = true;
			activeObject = objName;
			break;

		case "Lime": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isLimeAnalysis = true;
			activeObject = objName;
			break;

		case "Edible Soda": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isEdibleSodaAnalysis = true;
			activeObject = objName;
			break;

		case "Butter Milk": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isButterMilkAnalysis = true;
			activeObject = objName;
			break;

		case "Tomato": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnMenuButtonClick (objName);
			}
			isTomatoAnalysis = true;
			activeObject = objName;
			break;

		case "Soap": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isSoapAnalysis = true;
			activeObject = objName;
			break;

		case "Salt": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isSaltAnalysis = true;
			activeObject = objName;
			break;

		case "Washing Soda": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isWashingSodaAnalysis = true;
			activeObject = objName;
			break;

		case "Curd": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isCurdAnalysis = true;
			activeObject = objName;
			break;

		case "Tamarind": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isTamarindAnalysis = true;
			activeObject = objName;
			break;

		case "Citrate": 		
			if (!activeFromBowl) 
			{
				activeFromBowl = false;
				transform.GetComponent<General> ().OnObjectDrag (objName);
			}
			isCitrateAnalysis = true;
			activeObject = objName;
			break;

		default:
			break;
		}

	}

	public void ResetObject()
	{
		isLemonAnalysis = false;
		isSugarAnalysis = false;
		isLimeAnalysis = false;
		isEdibleSodaAnalysis = false;
		isButterMilkAnalysis = false;
		isTomatoAnalysis = false;
		isSoapAnalysis = false;
		isSaltAnalysis = false;
		isWashingSodaAnalysis = false;
		isCurdAnalysis = false;
		isTamarindAnalysis = false;
		isCitrateAnalysis = false;
	}

	public void OnAnalysisButtonClick(string name)
	{
		switch (name) 
		{
		case "Red":
			SetPlayerPrefsAns (activeObject,name);
			print (name);
			break;

		case "Blue" :
			SetPlayerPrefsAns (activeObject,name);
			print (name);
			break;
			
		case "None" :
			SetPlayerPrefsAns (activeObject,name);
			print (name);
			break;

		case "Acid":
			SetPlayerPrefsAns (activeObject,name);
			print (name);
			break;

		case "Base" :
			SetPlayerPrefsAns (activeObject,name);
			print (name);
			break;

		case "Nutral" :
			SetPlayerPrefsAns (activeObject,name);
			print (name);
			break;
		}
	}

	void SetPlayerPrefsAns(string objPlayerPrefsName, string ans)
	{
		PlayerPrefs.SetString (objPlayerPrefsName, ans);
	}

	string GetPlayerPrefsAns(string objPlayerPrefsName)
	{
		return PlayerPrefs.GetString (objPlayerPrefsName);
	}

//	public void OnAnalysisButtonClick(string name)
//	{
//		PlayerPrefs.SetString (this.gameObject.name, name);
//		print (name);
//		print (this.gameObject.name);
//	}
}
