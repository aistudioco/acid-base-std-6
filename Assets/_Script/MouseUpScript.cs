﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseUpScript : MonoBehaviour 
{
	Vector3 initialPosition;

	void Start()
	{
		initialPosition = this.gameObject.transform.position;
	}

	void OnMouseUp()
	{
		this.gameObject.transform.position = initialPosition;	
	}
}
