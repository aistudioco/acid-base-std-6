﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class General : MonoBehaviour 
{
	public Text instructionText;
	public Text canvasText;

	public Text emptyCanvas;

	public GameObject handDrag;
	public GameObject litmusPaperHandDrag;
	public GameObject tap;

	public static int i = 0;

	void Awake()
	{
		instructionText.text = "Select Ingrediants from the Menu..";
	}

	public void OnMenuButtonClick(string name)
	{
		tap.SetActive (true);
		instructionText.text = "Tap on "+ name ;
		canvasText.text = "Water";
	}

	public void OnObjectDrag(string name)
	{
		tap.SetActive (false);
		instructionText.text = "Drag " + name + " and put into Bowl..";
		handDrag.SetActive (true);
	}

	public void ObjectWater()
	{
		handDrag.SetActive (false);
		instructionText.text = "Select Litmus Paper";
	}

	public void OnLitmusPaperDrag()
	{
		instructionText.text = "Drag Litmus paper and put into Bowl..";
		litmusPaperHandDrag.SetActive (true);
	}

	public void CheckEffect()
	{
		litmusPaperHandDrag.SetActive (false);

		if (i < 2) 
		{
			instructionText.text = "Click On Another Listmus Paper..";
		}

		if (i > 2) 
		{
			instructionText.text = "Analyse the objcet is Acid or Base or Nutral..";	
			FindObjectOfType<CanvasPopUp> ().CanvasPopUpDisplay ();
		}
	}

	public void DebugMsg(string msg)
	{
		Debug.Log (msg);
	}

	public void OnButtonClickToFalseBool()
	{
		DragObject.acidicObject = false;
		DragObject.baseObject = false;
		DragObject.nutralObject = false;
	}

	public void ShowInstruction(string name )
	{
		emptyCanvas.text = name;
	}

	public void ResetCanvasName()
	{
		canvasText.text = "Water";
		DragObject.acidicObject = false;
		DragObject.baseObject = false;
		DragObject.nutralObject = false;
	}

	public void changeCanvasName(string name)
	{
		canvasText.text = name;
	}
}
