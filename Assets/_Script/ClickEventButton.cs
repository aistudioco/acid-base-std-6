﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickEventButton : MonoBehaviour 
{

	public GameObject[] setActiveButton;
	public GameObject[] setDeactiveButton;

	public void OnMenuButtonClick()
	{
		
		for (int i = 0; i < setActiveButton.Length; i++) 
		{
			setActiveButton [i].SetActive (true);
		}

		for (int i = 0; i < setDeactiveButton.Length; i++) 
		{
			setDeactiveButton [i].SetActive (false);
		}
	}
}
