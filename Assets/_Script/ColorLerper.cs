﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorLerper : MonoBehaviour 
{
	public Renderer redLitmusPaper, blueLitmusPaper;
	public Color red, blue;
	bool redLitmus = false, blueLitmus = false;

//	public float duration = 1f;

	void Start () 
	{
		redLitmusPaper = GetComponent<Renderer>();
		blueLitmusPaper = GetComponent<Renderer> ();
	}

//	void Update () 
//	{
//		if (DragObject.acidicObject && DragObject.litmusPaperDrag ) 
//		{
//			float lerp = Mathf.PingPong (Time.time, duration) / duration;
//			redLitmusPaper.material.color = Color.Lerp (startColor, endColor, lerp);
//			print ("color");
//			DragObject.acidicObject = false;
//		}
//	}

	public void OnRedLitmusButtonClick()
	{
		redLitmusPaper.material.color = red;

		FindObjectOfType<General> ().OnLitmusPaperDrag ();
	}

	public void OnBlueLitmusButtonClick()
	{
		blueLitmusPaper.material.color = blue;

		FindObjectOfType<General> ().OnLitmusPaperDrag ();
	}
		
	void OnTriggerEnter(Collider col)
	{
		if ((col.gameObject.CompareTag ("Bowl_Parent") && DragObject.acidicObject && DragObject.blueLitmusPaper) || 
			(col.gameObject.CompareTag ("Bowl_Parent") && DragObject.blueLitmusPaper && col.gameObject.GetComponent<DragObject>().acidObj ))
		{
			blueLitmusPaper.material.color = red;

			print ("Acidic Object");
			blueLitmus = true;
			redLitmus = false;
		}

		if ((col.gameObject.CompareTag ("Bowl_Parent") && DragObject.baseObject && DragObject.redLitmusPaper) ||
			(col.gameObject.CompareTag ("Bowl_Parent") && DragObject.redLitmusPaper && col.gameObject.GetComponent<DragObject>().baseObj ))
		{
			redLitmusPaper.material.color = blue;

			print ("Base Object");
			redLitmus = true;
			blueLitmus = false;
		}

		if ((col.gameObject.CompareTag ("Bowl_Parent") && DragObject.nutralObject) || 
			(col.gameObject.CompareTag("Bowl_Parent") && col.gameObject.GetComponent<DragObject>().nutralObj)) 
		{
			print ("Nutral Object");
		}
	}
}
